/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.exod.lipo.controller;

import com.exod.lipo.constante.JSPConstants;
import com.exod.lipo.constante.RouteConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Roussel
 */
@Controller
public class HomeController extends AbstractController{
    
    @RequestMapping(RouteConstants.HOME_ROUTE)
    public ModelAndView homeController(){
        return new ModelAndView(JSPConstants.HOME_JSP);
        
    }
            
}
