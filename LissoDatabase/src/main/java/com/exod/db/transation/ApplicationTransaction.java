/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.exod.db.transation;

import com.exod.db.model.Application;
import com.exod.db.wrapper.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Roussel
 */
public class ApplicationTransaction {
    public Application getApplication(int indexApp){
        Session session = HibernateUtil.currentSession();
        return (Application) session.get(Application.class, new Integer(indexApp));
    }
    
    public void saveApplication(Application application){
        Session session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.save(application);
        session.getTransaction().commit();
    }
    
    public Application getApplicationByName(String name){
        Session session = HibernateUtil.currentSession();
        return (Application) session.createCriteria(Application.class)
                .add(Restrictions.eq("name", name)).list().get(0);
    }
}
