/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.exod.db.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Roussel
 */
@Entity
@Table(name="resource")
public class Resource implements Serializable {
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "url")
    private String url;
    @Column(name = "id_fonctionnel")
    private String id_fonctionnel;
    @ManyToOne
    private ResourceType resource_type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ResourceType getResource_type() {
        return resource_type;
    }

    public void setResource_type(ResourceType resource_type) {
        this.resource_type = resource_type;
    }

    public String getId_fonctionnel() {
        return id_fonctionnel;
    }

    public void setId_fonctionnel(String id_fonctionnel) {
        this.id_fonctionnel = id_fonctionnel;
    }
    
    
}
