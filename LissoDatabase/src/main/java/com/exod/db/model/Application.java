/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exod.db.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 *
 * @author Roussel
 */
@Entity
@Table(name = "application")
public class Application implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    private ApplicationType application_type;

    @Column(name = "id_fonctionnel")
    private String id_fonctionnel;

    @Column(name = "language", nullable = true)
    private String language;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="application_has_resource", 
            joinColumns = @JoinColumn(name="application_id"), 
            inverseJoinColumns=@JoinColumn(name="resource_id"))
    private final Set<Resource> resources = new HashSet<Resource>();
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ApplicationType getApplication_type() {
        return application_type;
    }

    public void setApplication_type(ApplicationType application_type) {
        this.application_type = application_type;
    }

    public String getId_fonctionnel() {
        return id_fonctionnel;
    }

    public void setId_fonctionnel(String id_fonctionnel) {
        this.id_fonctionnel = id_fonctionnel;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Set<Resource> getResources() {
        return resources;
    }
}
