<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="${application.language}">

    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>${application.name}</title>
            <c:out value="${application.resources}"/>
            <c:forEach var="resource" items="${application.resources}" >
                <c:out value="${resourc}"/>
                <c:out value="${resource.resource_type.label }"/>
                    
                <c:choose>
                    <c:when test="${resource.resource_type.label == 'javascript' }">
                        <script type="text/javascript" src="scripts/${resource.url}"/>
                    </c:when>
                    <c:when test="${resource.resource_type.label == 'css' }">
                        <link rel="stylesheet" type="text/css" href="css/${resource.url}">
                    </c:when>
                    <c:when test="${resource.resource_type.label == 'template' }">
                        <script type="text/html" src="templates/${resource.url}"/>
                    </c:when>
                    
                </c:choose>
            </c:forEach>
        </head>
        <body>
            <h1>Hello Exod!</h1>
        </body>
    </html>
